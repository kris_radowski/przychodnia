package com.example.aplikacja_przychodnia.service;

import com.example.aplikacja_przychodnia.DataModelAndRepo.Patient;
import com.example.aplikacja_przychodnia.DataModelAndRepo.PatientRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientService {
    private UserRepository userRepository;
    private PatientRepository patientRepository;

    public PatientService(){
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPatientRepository(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public void addPatient(Patient patient){
        patientRepository.save(patient);
    };
}
