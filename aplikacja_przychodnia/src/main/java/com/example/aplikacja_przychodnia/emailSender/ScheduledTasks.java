package com.example.aplikacja_przychodnia.emailSender;

import com.example.aplikacja_przychodnia.DataModelAndRepo.Visit;
import com.example.aplikacja_przychodnia.DataModelAndRepo.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class ScheduledTasks {
    private EmailServiceImpl emailService;
    private VisitRepository visitRepository;

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    //codziennie o 1 w nocy
//    @Scheduled(cron = "0 0 1 * * ?")


    @Autowired
    public ScheduledTasks(EmailServiceImpl emailService, VisitRepository visitRepository) {
        this.emailService=emailService;
        this.visitRepository=visitRepository;
    }


    //co minutę
//    @Scheduled(cron = "0 * * ? * *")

    //codziennie o 10:00
    @Scheduled(cron = "0 0 12 * * ? ")
    public void reportCurrentTime() {
        List<Visit> matchingVisits = visitRepository.findVisitsTwoDaysFromToday(LocalDateTime.now());
        System.out.println("----------------------------------------------------------------------");
        for (Visit visit : matchingVisits) {

            String text = "Wiadomość wygenerowano automatycznie. Prosimy nie odpowiadać." + "\n"+"\n"+"Drogi Pacjencie, "+ "\n"+"\n"+
                    "Przypominamy, że dnia "+visit.getVisitDateTime().toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate().format(dateFormatter)+" o godzinie "+visit.getVisitDateTime().toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime().format(timeFormatter) +" masz zaplanowaną wizytę u lekarza "+visit.getDoctor().getProfTitle()+ " "+
                    visit.getDoctor().getUser().getFirstName()+" "+visit.getDoctor().getUser().getLastName()+".";
            System.out.println(text);
            System.out.println("----------------------------------------------------------------------");

            emailService.sendSimpleMessage(visit.getPatient().getUser().getEmail(),"Przypomnienie o nadchodzącej wizycie",text);

        }
        System.out.println("The time is now " + LocalDateTime.now().format(dateTimeFormatter));
    }
    public void toDate() {}

}
