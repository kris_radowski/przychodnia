package com.example.aplikacja_przychodnia.ui.views.forms;

import com.example.aplikacja_przychodnia.DataModelAndRepo.*;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomDoctorObject;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.example.aplikacja_przychodnia.service.VisitService;
import com.example.aplikacja_przychodnia.ui.views.dialogs.DialogRegisterVisitByReceptionist;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

@PageTitle("Znajdź lekarza")
@Route(value = "findDoctorReceptionist", layout = RegisteredMenuBar.class)
public class FormFindDoctorReceptionist extends VerticalLayout {
    Grid<CustomDoctorObject> grid;
    ComboBox<String> lastName,specialization;

    private Button btnNextPage, btnPreviousPage;
    private  int page=0;
    private int size=5;
    private HorizontalLayout horizontalLayout1;
    private int totalPages=0;
    private H5 currentPage;

    @Autowired
    public FormFindDoctorReceptionist(DoctorRepository doctorRepository, VisitService visitService, PatientRepository patientRepository , VisitRepository visitRepository, UserRepository userRepository){
        lastName = new ComboBox<>("Nazwisko");
        lastName.setClearButtonVisible(true);
        specialization = new ComboBox<>("Specjalizacja");
        specialization.setClearButtonVisible(true);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        fillCBLastName(doctorRepository,lastName);
        horizontalLayout.add(lastName);
//        add(lastName);
        fillCBSpecialization(doctorRepository,specialization);
        horizontalLayout.add(specialization);
//        add(specialization);
        add(horizontalLayout);
        Button findDoctor = new Button("Szukaj");
        findDoctor.addClickListener(click -> {
            page=0;
            if(lastName.getValue()!=null){
                remove(grid);
                if(specialization.getValue()!=null){
                    fillTableByDoctorLastNameAndSpecialization(doctorRepository,specialization.getValue(),lastName.getValue());
                } else {
                    fillTableByDoctorLastName(doctorRepository);
                }
                add(grid);
            } else if (specialization.getValue()!=null){
                remove(grid);
                fillTableByDoctorSpecialization(doctorRepository);
                add(grid);
            }
            if (lastName.getValue()==null && specialization.getValue()==null) {
                fillTableWithAllExistingDoctors(doctorRepository);
            }
//            else { Notification.show("Nie wybrano lekarza",3000, Notification.Position.MIDDLE); }
        });
        findDoctor.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        add(findDoctor);

        btnNextPage = new Button("Następna strona");
        btnPreviousPage = new Button("Poprzednia strona");
        currentPage = new H5();

        btnNextPage.addClickListener(event -> {
            System.out.println("----------------");
            System.out.println("strona"+page);
            if (page<totalPages-1) {
                page+=1;
                if(lastName.getValue()!=null){
                    remove(grid);
                    if(specialization.getValue()!=null){
                        fillTableByDoctorLastNameAndSpecialization(doctorRepository,specialization.getValue(),lastName.getValue());
                    } else {
                        fillTableByDoctorLastName(doctorRepository);
                    }
                    add(grid);
                } else if (specialization.getValue()!=null){
                    remove(grid);
                    fillTableByDoctorSpecialization(doctorRepository);
                    add(grid);
                }
                if (lastName.getValue()==null && specialization.getValue()==null) {
                    fillTableWithAllExistingDoctors(doctorRepository);
                }

            } /*addTableWithPagination(page+=1,size,visitRepository);*/
            System.out.println("strona"+page);
            currentPage.setText(page+1+" z "+totalPages);
        });
        btnPreviousPage.addClickListener(event -> {

            if (page>0) {
                page-=1;
                if(lastName.getValue()!=null){
                    remove(grid);
                    if(specialization.getValue()!=null){
                        fillTableByDoctorLastNameAndSpecialization(doctorRepository,specialization.getValue(),lastName.getValue());
                    } else {
                        fillTableByDoctorLastName(doctorRepository);
                    }
                    add(grid);
                } else if (specialization.getValue()!=null){
                    remove(grid);
                    fillTableByDoctorSpecialization(doctorRepository);
                    add(grid);
                }
                if (lastName.getValue()==null && specialization.getValue()==null) {
                    fillTableWithAllExistingDoctors(doctorRepository);
                }
            }
            /*addTableWithPagination(page-=1,size,visitRepository);*/
            System.out.println("strona"+page);
            currentPage.setText(page+1+" z "+totalPages);
        });
        horizontalLayout1 = new HorizontalLayout();

        horizontalLayout1.add(btnPreviousPage,currentPage,btnNextPage);
//        addTableWithPagination(page,size,visitRepository);
        currentPage.setText(page+1+" z "+totalPages);
        add(horizontalLayout1);


        grid = new Grid<>();
        fillTableWithAllExistingDoctors(doctorRepository);
        grid.addColumn(CustomDoctorObject::getProfessionalTitle).setHeader("Tytuł").setAutoWidth(true);
        grid.addColumn(CustomDoctorObject::getFirstName).setHeader("Imię").setAutoWidth(true);
        grid.addColumn(CustomDoctorObject::getLastName).setHeader("Nazwisko").setAutoWidth(true);
        grid.addColumn(CustomDoctorObject::getSpecialization).setHeader("Specjalizacja").setAutoWidth(true);
//        grid.removeColumnByKey("doctorId");
        grid.addComponentColumn(doctor -> {
            Button button = new Button("Umów na wizytę");
            button.addClickListener(click ->{
//                UI.getCurrent().getPage().setLocation("registerVisit?doctorid="+doctor.getDoctorId()+"&patientid=1");

                new DialogRegisterVisitByReceptionist(true,doctor.getDoctorId(),visitService,doctorRepository,patientRepository,visitRepository,userRepository).open();

                //Notification.show(Long.toString(doctor.getDoctorId()),1000, Notification.Position.BOTTOM_START);
            });
            return button;
        }).setHeader("");
        add(grid);
    }

    private void fillTableByDoctorLastNameAndSpecialization(DoctorRepository doctorRepository, String specialization, String lastName) {
        Page<Doctor> doctorsByLastName = doctorRepository.findAllByLastNameAndSpecialization(lastName,specialization, PageRequest.of(page,size));
        totalPages = doctorsByLastName.getTotalPages();
        currentPage.setText(page+1+" z "+totalPages);

        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        doctorsByLastName.forEach(doctor ->
                customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName())));

        if (!customDoctorObjects.isEmpty()) {
            grid.setItems(customDoctorObjects);
        }
        else { Notification.show("Nie ma takiego lekarza",1000, Notification.Position.MIDDLE);}
    }

    private void fillTableByDoctorSpecialization(DoctorRepository doctorRepository) {
        Page<Doctor> doctorSpecializationsAndTitle = doctorRepository.findDoctorsBySpecialization(specialization.getValue(),PageRequest.of(page,size));
        totalPages = doctorSpecializationsAndTitle.getTotalPages();
        currentPage.setText(page+1+" z "+totalPages);

        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        doctorSpecializationsAndTitle.forEach(doctor ->
                customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName())));

        if (!customDoctorObjects.isEmpty()) {
            grid.setItems(customDoctorObjects);
//            grid.addColumn(new ComponentRenderer<>(doctor -> new Button("Umów się na wizytę"))).setHeader("");
        }
        else { Notification.show("Nie ma takiego lekarza",1000, Notification.Position.MIDDLE); }
    }

    private void fillTableByDoctorLastName(DoctorRepository doctorRepository){
        Page<Doctor> doctorsByLastName = doctorRepository.findDoctorsByLastName(lastName.getValue(),PageRequest.of(page,size));
        totalPages = doctorsByLastName.getTotalPages();
        currentPage.setText(page+1+" z "+totalPages);

        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        doctorsByLastName.forEach(doctor ->
                customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName())));

        if (!customDoctorObjects.isEmpty()) {
            grid.setItems(customDoctorObjects);
        }
        else { Notification.show("Nie ma takiego lekarza",1000, Notification.Position.MIDDLE);}
    }

    private void fillTableWithAllExistingDoctors(DoctorRepository doctorRepository) {
        Page<Doctor> listAllExistingDoctors = doctorRepository.findAllDoctors(PageRequest.of(page,size));
        totalPages=listAllExistingDoctors.getTotalPages();
        currentPage.setText(page+1+" z "+totalPages);

        List<CustomDoctorObject> customDoctorObjects = new ArrayList<>();
        listAllExistingDoctors.forEach(doctor -> {
            customDoctorObjects.add(new CustomDoctorObject(doctor.getId(),doctor.getSpecialization(),doctor.getProfTitle(),doctor.getUser().getFirstName(),doctor.getUser().getLastName()));});
        grid.setItems(customDoctorObjects);
    }

    private void fillCBLastName(DoctorRepository doctorRepository, ComboBox<String> lastName) {
        List<String> surnames = doctorRepository.findAllUsernames();
        if(!surnames.isEmpty()) lastName.setItems(surnames);
        System.out.println(lastName);
    }

    private void fillCBSpecialization(DoctorRepository doctorRepository, ComboBox<String> specialization) {
        List<String> specialzationList = doctorRepository.findAllSpecializations();
        specialization.setItems(specialzationList);
    }
}

