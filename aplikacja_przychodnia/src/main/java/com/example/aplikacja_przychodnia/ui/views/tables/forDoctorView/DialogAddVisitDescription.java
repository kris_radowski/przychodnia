package com.example.aplikacja_przychodnia.ui.views.tables.forDoctorView;

import com.example.aplikacja_przychodnia.DataModelAndRepo.DoctorRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.PatientRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.VisitRepository;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * tu będzie dodawany komentarz do wizyty i będzie ustawiane, że wizyta się odbyła
 * */

@Transactional
public class DialogAddVisitDescription extends Dialog {


    public DialogAddVisitDescription(Date visitDateTime, Long userId, String currentlyLoggedInUserEmail, VisitRepository visitRepository, DoctorRepository doctorRepository, String existingDescription, PatientRepository patientRepository, boolean isFinished) {
//        System.out.println(isFinished);

        setCloseOnEsc(true);
        setCloseOnOutsideClick(true);
        setWidthFull();


        TextArea textArea;
        textArea = new TextArea();
        VerticalLayout verticalLayout;
        if (!isFinished) {

            textArea.setWidth("300px");
            textArea.setHeight("400px");
            //jesli nie ma opisu, to dodaj istniejacy opis
            if (existingDescription==null) ;
            else textArea.setValue(existingDescription);

            verticalLayout = new VerticalLayout();

            verticalLayout.add(textArea);
            add(verticalLayout);
        } else {
            Span span1 = new Span();
            if (existingDescription==null) span1.setText("Brak opisu");
            else span1.setText(existingDescription);
            verticalLayout = new VerticalLayout();

            verticalLayout.add(span1);
            add(verticalLayout);

        }



        VerticalLayout layout = new VerticalLayout();
        layout.setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.STRETCH);
        layout.setSizeFull();

        Button btnAddComment = new Button("Dodaj komentarz");
        Button btnPastVisit = new Button("Wizyta odbyła się");

        if (!isFinished) {
            add(btnAddComment);
            add(btnPastVisit);
        }


//funkcja dodania komentarza
        btnAddComment.addClickListener(event -> {
            Long doctorId = doctorRepository.findDoctorIdByEmail(currentlyLoggedInUserEmail);
            Long patientId = patientRepository.findPatientIdByUserId(userId);
            System.out.println("doctor id: "+doctorId);
            System.out.println("patient id: "+patientId);
            System.out.println("visitDatetime "+visitDateTime);
            System.out.println("comment"+textArea.getValue());

            if (!textArea.getValue().isEmpty()) visitRepository.addCommentToVisit(visitDateTime,doctorId,patientId,textArea.getValue());

            Notification.show("Zmieniono opis",1000, Notification.Position.MIDDLE);
            UI.getCurrent().getPage().reload();

        });
// funkcja poprzedniej wizyty
        btnPastVisit.addClickListener(event -> {
            Long doctorId = doctorRepository.findDoctorIdByEmail(currentlyLoggedInUserEmail);
            Long patientId = patientRepository.findPatientIdByUserId(userId);
            System.out.println("doctor id: "+doctorId);
            System.out.println("patient id: "+patientId);
            System.out.println("visitDatetime "+visitDateTime);
            System.out.println("comment"+textArea.getValue());

            visitRepository.addVisitAsPast(visitDateTime,doctorId,patientId);

            Notification.show("Przeniesiono wizytę",1000, Notification.Position.MIDDLE);
            UI.getCurrent().getPage().reload();

        });


    }


}
