package com.example.aplikacja_przychodnia.ui.views.tables.forPatientView;

import com.example.aplikacja_przychodnia.DataModelAndRepo.*;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomAllRegisteredVisitsList;
import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

@PageTitle("Znajdź lekarza")
@Route(value = "registeredVisitsCurrentPatient", layout = RegisteredMenuBar.class)
public class RegisteredVisitsPerPatient extends VerticalLayout {

    Page<Visit> visitList;
    private Button btnNextPage, btnPreviousPage;
    private  int page=0;
    private int size=5;
    private HorizontalLayout horizontalLayout;
    private int totalPages=0;

    @Autowired
    public RegisteredVisitsPerPatient(VisitRepository visitRepository){
        add(new H2("Nadchodzące wizyty"));


        btnNextPage = new Button("Następna strona");
        btnPreviousPage = new Button("Poprzednia strona");
        H5 currentPage = new H5();

        btnNextPage.addClickListener(event -> {
            System.out.println("----------------");
            System.out.println("strona"+page);
            if (page<totalPages-1) addTableWithPagination(page+=1,size,visitRepository);
            System.out.println("strona"+page);
            currentPage.setText(page+1+" z "+totalPages);
        });
        btnPreviousPage.addClickListener(event -> {

            if (page>0) addTableWithPagination(page-=1,size,visitRepository);
            System.out.println("strona"+page);
            currentPage.setText(page+1+" z "+totalPages);
        });
        horizontalLayout = new HorizontalLayout();



        horizontalLayout.add(btnPreviousPage,currentPage,btnNextPage);
        addTableWithPagination(page,size,visitRepository);
        currentPage.setText(page+1+" z "+totalPages);






    }

    private void addTableWithPagination(int page, int size, VisitRepository visitRepository) {
        removeAll();
        add(new H2("Nadchodzące wizyty"));
        add(horizontalLayout);


        FindUserData findUserData = new FindUserData();
        String currentlyLoggedInUserEmail = findUserData.findCurrentlyLoggedInUserEmail();
        System.out.println(currentlyLoggedInUserEmail + " adres email");
        List<CustomAllRegisteredVisitsList> customAllRegisteredVisitsLists = new ArrayList<>();

        visitList = visitRepository.findAllVisitsForUser(currentlyLoggedInUserEmail, PageRequest.of(page, size));
        totalPages=visitList.getTotalPages();
        customAllRegisteredVisitsLists.clear();

        System.out.println(visitList+ " lista wizyt przed opakowaniem w custom pojo");
        if (!visitList.isEmpty()) {
            visitList.forEach(visit -> {
                customAllRegisteredVisitsLists.add(new CustomAllRegisteredVisitsList(visit.getVisitDateTime(),
                        visit.getDoctor().getProfTitle(),visit.getDoctor().getUser().getFirstName(),
                        visit.getDoctor().getUser().getLastName(),
                        visit.getDoctor().getSpecialization()
                ));
            });

            Grid<CustomAllRegisteredVisitsList> visitGrid = new Grid<>();
            visitGrid.setItems(customAllRegisteredVisitsLists);
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getCustomVisitDate).setHeader("Data wizyty");
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getCustomVisitTime).setHeader("Godzina wizyty");
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getDoctorWithTitle).setHeader("Lekarz");
            visitGrid.addColumn(CustomAllRegisteredVisitsList::getSpecialization).setHeader("Specjalizacja");;



            add(visitGrid);

        }
        else add(new H2("Nie ma jeszcze zaplanowanych wizyt"));
        System.out.println(customAllRegisteredVisitsLists.toString());
    }

}
