package com.example.aplikacja_przychodnia.ui.views;
//

import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;


@Route(value = "accessDenied",layout = RegisteredMenuBar.class)
public class AccessDeniedPage extends VerticalLayout {

    public AccessDeniedPage() {
//        VerticalLayout v = new VerticalLayout();

        Icon logo = new Icon(VaadinIcon.EXCLAMATION_CIRCLE);
        logo.setSize("300px");
        logo.setColor("red");

        add(logo);

        H1 h1 = new H1("ACCESS DENIED!");
//        Button redirectBtn = new Button("Powrót do strony głównej");
//        redirectBtn.addClickListener(buttonClickEvent -> {
//            UI.getCurrent().navigate("");
//        });

        add(h1);
//        add(redirectBtn);
        setAlignItems(Alignment.CENTER);

    }
}
