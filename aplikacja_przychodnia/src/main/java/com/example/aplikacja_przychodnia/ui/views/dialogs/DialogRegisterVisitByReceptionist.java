package com.example.aplikacja_przychodnia.ui.views.dialogs;

import com.example.aplikacja_przychodnia.DataModelAndRepo.*;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomDoctorObject;
import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.service.VisitService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.*;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.combobox.GeneratedVaadinComboBox;
import sun.font.TextLabel;


import javax.transaction.Transactional;
import java.awt.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Transactional
public class DialogRegisterVisitByReceptionist extends Dialog {
    private DatePicker visitDate;
    private TimePicker visitTime;
    //private ComboBox<String> lastName;
    private TextField lastName;
    public String nowa;

    public DialogRegisterVisitByReceptionist(boolean newInstance, Long doctorId, VisitService visitService, DoctorRepository doctorRepository, PatientRepository patientRepository , VisitRepository visitRepository, UserRepository userRepository) {

        setCloseOnEsc(true);
        setCloseOnOutsideClick(true);
        setWidthFull();

        VerticalLayout layout = new VerticalLayout();
        layout.setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.STRETCH);
        layout.setSizeFull();

//        TextField textField = new TextField("Id");
//        textField.setWidth("400px");
//        if (!doctorId.toString().isEmpty()) textField.setValue(doctorId.toString());
//        add(textField);

        visitDate = new DatePicker("Data wizyty");
        visitTime = new TimePicker("Godzina wizyty");
        lastName = new TextField("Nazwisko");
        Button register = new Button("Potwierdź");
        //fillPatientLastName(patientRepository,lastName);

        lastName.addValueChangeListener(event -> {
            event.getValue();
            nowa = event.getValue();
            System.out.println("test0 " + lastName + " test1 " + nowa);
        register.addClickListener(click -> {
                Patient patient = patientRepository.findPatientByLastName(nowa);
                FindUserData findUserData = new FindUserData();
                Doctor doctor = doctorRepository.findDoctorById(doctorId);

                System.out.println("-------------------------------------------------------------------");
                System.out.println("test2" + lastName);
                System.out.println("-------------------------------------------------------------------");
                System.out.println(nowa);
                System.out.println("-------------------------------------------------------------------");
                System.out.println(patient);
                //System.out.println(patient.toString());
                //System.out.println(doctor.toString());

                if (!DoctorIsBusy(visitRepository, doctor, visitDate, visitTime)) {
                    registerVisit(visitService, patient, doctor);
                    Notification.show("Zapisano na wizytę dnia " + visitDate.getValue() + " o godz. " + visitTime.getValue(), 3000, Notification.Position.MIDDLE);
                } else Notification.show("Ktoś już jest zapisany na ten termin...", 3000, Notification.Position.MIDDLE);
            });});

        VerticalLayout v  = new VerticalLayout();
        v.add(visitDate,visitTime,register,lastName);
        add(v);
        visitDate.setI18n(
                new DatePicker.DatePickerI18n().setWeek("tydzień").setCalendar("kalendarz")
                        .setClear("Wyczyść").setToday("dzisiaj")
                        .setCancel("Anuluj").setFirstDayOfWeek(1)
                        .setMonthNames(Arrays.asList("styczeń", "luty",
                                "marzec", "kwiecień", "maj", "czerwiec",
                                "lipiec", "sierpień", "wrzesień", "październik",
                                "listopad", "grudzień")).setWeekdays(
                        Arrays.asList("niedziela", "poniedziałek", "wtorek",
                                "środa", "czwartek", "piątek",
                                "sobota")).setWeekdaysShort(
                        Arrays.asList("nd.", "pon.", "wt.", "śr.", "czw.", "pt.",
                                "sob.")));
    }

    private boolean DoctorIsBusy(VisitRepository visitRepository, Doctor doctor, DatePicker visitDate, TimePicker visitTime) {
        List<Visit> visits = visitRepository.getAllByDoctorAndVisitDateTime(doctor,
                ConvertLocalDateTimeToDate(visitDate.getValue().atTime(visitTime.getValue())));
        return visits.size() != 0;
    }

    private void registerVisit(VisitService visitService, Patient patient, Doctor doctor) {
//        System.out.println(patient.toString());
        //System.out.println(doctor.toString());
        Visit v = new Visit();
        v.setPatient(patient);
        v.setDoctor(doctor);
        Date visitDateTime = ConvertLocalDateTimeToDate(visitDate.getValue().atTime(visitTime.getValue()));
        v.setVisitDateTime(visitDateTime);
        visitService.addVisit(v);
    }

    private Date ConvertLocalDateTimeToDate(LocalDateTime atTime) {
        return Date.from(atTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /*private void fillPatientLastName(PatientRepository patientRepository, ComboBox<String> lastName) {
        List<String> surnames = new ArrayList<>();
        List<User> userSecondNameList = patientRepository.findAllUsernames();
        System.out.println(userSecondNameList.toString());
        userSecondNameList.forEach(user -> {
//            String aaaaa = user.getLastName();
//            System.out.println(user.getLastName());
            if (!user.getLastName().isEmpty()) surnames.add(user.getLastName());
        });
        if (!surnames.isEmpty()) lastName.setItems(surnames);
        System.out.println("test1"+lastName);
    }*/

}