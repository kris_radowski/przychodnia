package com.example.aplikacja_przychodnia.ui.views.tables.forDoctorView;

import com.example.aplikacja_przychodnia.DataModelAndRepo.DoctorRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.PatientRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.Visit;
import com.example.aplikacja_przychodnia.DataModelAndRepo.VisitRepository;
import com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo.CustomAllVisitsForDoctor;
import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;


@PageTitle("Umówione wizyty")
@Route(value = "visitsDoctor", layout = RegisteredMenuBar.class)
public class RegisteredVisitsPerDoctor  extends VerticalLayout {
    private Page<Visit> visitList;
    private Button btnNextPage, btnPreviousPage;
    private  int page=0;
    private int size=5;
    private HorizontalLayout horizontalLayout;
    private int totalPages=0;

    @Autowired
    public RegisteredVisitsPerDoctor(VisitRepository visitRepository, DoctorRepository doctorRepository, PatientRepository patientRepository) {
        add(new H1("Zaplanowane wizyty"));
        btnNextPage = new Button("Następna strona");
        btnPreviousPage = new Button("Poprzednia strona");
        H5 currentPage = new H5();

        btnNextPage.addClickListener(event -> {
            System.out.println("----------------");
            System.out.println("strona"+page);
            if (page<totalPages-1) addTableWithPagination(page+=1,size,visitRepository,doctorRepository,patientRepository);
            System.out.println("strona"+page);
            currentPage.setText(page+1+" z "+totalPages);
        });
        btnPreviousPage.addClickListener(event -> {

            if (page>0) addTableWithPagination(page-=1,size,visitRepository,doctorRepository,patientRepository);
            System.out.println("strona"+page);
            currentPage.setText(page+1+" z "+totalPages);
        });
        horizontalLayout = new HorizontalLayout();



        horizontalLayout.add(btnPreviousPage,currentPage,btnNextPage);
        addTableWithPagination(page,size,visitRepository,doctorRepository,patientRepository);
        currentPage.setText(page+1+" z "+totalPages);





    }

    private void addTableWithPagination(int page, int size, VisitRepository visitRepository,
                                        DoctorRepository doctorRepository, PatientRepository patientRepository) {

        removeAll();
        add(new H1("Zaplanowane wizyty"));
        add(horizontalLayout);

        FindUserData findUserData = new FindUserData();
        String currentlyLoggedInUserEmail = findUserData.findCurrentlyLoggedInUserEmail();
//        System.out.println(currentlyLoggedInUserEmail);

        List<CustomAllVisitsForDoctor> customAllRegisteredVisitsLists = new ArrayList<>();

        visitList = visitRepository.findAllVisitsForDoctor(currentlyLoggedInUserEmail,false, PageRequest.of(page, size));
        totalPages=visitList.getTotalPages();
        customAllRegisteredVisitsLists.clear();
//        System.out.println(visitList.toString());
        if (!visitList.isEmpty()) {
            add(new H3("Kliknij na wizytę, aby edytować"));

            visitList.forEach(visit -> {
                customAllRegisteredVisitsLists.add(new CustomAllVisitsForDoctor(visit.getVisitDateTime(),
                        visit.getPatient().getUser().getFirstName(),visit.getPatient().getUser().getLastName(),
                        visit.getDescription(),visit.getPatient().getUser().getId(),visit.isFinished()));
            });

            Grid<CustomAllVisitsForDoctor> visitGrid = new Grid<>();
            visitGrid.setItems(customAllRegisteredVisitsLists);
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomVisitDate).setHeader("Data wizyty");
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomVisitTime).setHeader("Godzina wizyty");
            visitGrid.addColumn(CustomAllVisitsForDoctor::getCustomPatientFormatted).setHeader("Pacjent");

            visitGrid.addItemClickListener(event -> {
//                Notification.show(event.getItem().toString(),1000, Notification.Position.MIDDLE);
                System.out.println(event.getItem().getVisitDateTime()+" | userEmail="+event.getItem().getUserId()
                        +" | doctorEmail="+currentlyLoggedInUserEmail);

                new DialogAddVisitDescription(event.getItem().getVisitDateTime(),event.getItem().getUserId(),currentlyLoggedInUserEmail,visitRepository,doctorRepository,event.getItem().getDescription(),patientRepository,event.getItem().isFinished()).open();
            });

            add(visitGrid);
        }
        else add(new H3("Brak zaplanowanych wizyty"));
    }
}
