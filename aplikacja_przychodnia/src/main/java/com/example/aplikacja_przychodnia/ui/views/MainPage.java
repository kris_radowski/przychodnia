package com.example.aplikacja_przychodnia.ui.views;


import com.example.aplikacja_przychodnia.FindUserData;
import com.example.aplikacja_przychodnia.navAndThemes.RegisteredMenuBar;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

@Secured({"ROLE_ADMIN","ROLE_USER","ROLE_RECEPTION","ROLE_DOCTOR"})
@PageTitle("Strona główna")
@Route(value = "", layout = RegisteredMenuBar.class)

public class MainPage extends VerticalLayout {


    @Autowired
    public MainPage() {
        //klasa z mewtodami do znajdowania danych o zalogowanych userach
        FindUserData findUserData = new FindUserData();
        H1 h1 = new H1("Witamy na stronie głównej");
        add(h1);
//        H3 h3 = new H3("Użytkownik: "+findUserData.findCurrentlyLoggedInUserEmail());
//        H2 h2 = new H2("Uprawnienia: "+findUserData.findUserRoles());
//        add(h3,h2);




    }



}
