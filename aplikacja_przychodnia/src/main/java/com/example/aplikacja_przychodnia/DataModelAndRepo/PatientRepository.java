package com.example.aplikacja_przychodnia.DataModelAndRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Long> {
    @Query(value = "SELECT * FROM przychodnia.patient where patient.user_id=:id",nativeQuery = true)
    Patient findPatientByUserId(Long id);

    @Query(value = "SELECT id FROM przychodnia.patient where patient.user_id=:id",nativeQuery = true)
    Long findPatientIdByUserId(Long id);

    @Transactional
    @Query(value = "SELECT * FROM przychodnia.patient where user_id=" +
            "(select user.user_id FROM przychodnia.user where email=:nowa)",nativeQuery = true)
    Patient findPatientByLastName(String nowa);

    @Query("SELECT DISTINCT p.user FROM Patient p")
    List<User> findAllUsernames();

    @Transactional
    @Query(value = "SELECT \n" +
            "patient.id,user.*\n" +
            "FROM przychodnia.patient, przychodnia.user\n" +
            "where patient.user_id=user.user_id\n",nativeQuery = true)
    List<Patient> findAllPatients();

    @Query(value = "select * from patient where patient.id=:patientId",nativeQuery = true)
    Patient findpatientById(Long patientId);

    @Query(value = "select patient.id from patient, user where patient.user_id=user.user_id\n" +
            "AND user.email=:email",nativeQuery = true)
    Long findPatientIdByEmail(String email);
}
//