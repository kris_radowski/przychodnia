package com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo;

/**
 * model danych do wyświetlania lekarzy w tabeli
 * */

public class CustomDoctorObject {
    private Long doctorId;
    private String specialization;
    private String professionalTitle;
    private String firstName;
    private String lastName;

    public CustomDoctorObject(Long doctorId, String specialization, String professionalTitle, String firstName, String lastName) {
        this.doctorId = doctorId;
        this.specialization = specialization;
        this.professionalTitle = professionalTitle;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getProfessionalTitle() {
        return professionalTitle;
    }

    public void setProfessionalTitle(String professionalTitle) {
        this.professionalTitle = professionalTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public  String getCustomDoctorNameSurNameTitle() {
        String customData = getProfessionalTitle()+" "+getFirstName()+" "+getLastName();
        return customData;
    }
}
