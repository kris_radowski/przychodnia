package com.example.aplikacja_przychodnia.DataModelAndRepo;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(  uniqueConstraints = {
        @UniqueConstraint(columnNames = {"id", "user_id"})
})
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;

    @OneToMany(mappedBy = "patient",fetch = FetchType.EAGER)
    private Set<Visit> visits;

    public Patient() {
    }

    public Patient(User user) {
        this.user = user;
    }

    public User getUser() { return user; }
    public void setUser(User user) { this.user = user; }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", user=" + user +
                ", visits=" + visits +
                '}';
    }
}

