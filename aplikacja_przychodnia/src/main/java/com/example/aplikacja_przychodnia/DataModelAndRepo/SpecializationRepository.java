package com.example.aplikacja_przychodnia.DataModelAndRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SpecializationRepository extends JpaRepository<Specialization,Long> {
    @Query("SELECT s.specialization FROM Specialization s")
    List<String> findAllSpecializations();
}
