package com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
/**
 * model danych do wyświetlania wizyt lekarskich wysortowanych dla danego pacjenta
 * */

public class CustomAllRegisteredVisitsList {
    private Date visitDateTime;
    private String profTitle;
    private String doctorFirstName;
    private String doctorLastName;
    private String specialization;

    public CustomAllRegisteredVisitsList(Date visitDateTime, String profTitle, String doctorFirstName, String doctorLastName, String specialization) {
        this.visitDateTime = visitDateTime;
        this.profTitle = profTitle;
        this.doctorFirstName = doctorFirstName;
        this.doctorLastName = doctorLastName;
        this.specialization = specialization;
    }

    public Date getVisitDateTime() {
        return visitDateTime;
    }

    public void setVisitDateTime(Date visitDateTime) {
        this.visitDateTime = visitDateTime;
    }

    public String getProfTitle() {
        return profTitle;
    }

    public void setProfTitle(String profTitle) {
        this.profTitle = profTitle;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getSpecialization() { return specialization; }

    public void setSpecialization(String specialization) { this.specialization = specialization; }

    public String getDoctorWithTitle() {
        String doctor = getProfTitle()+" "+getDoctorFirstName()+" "+getDoctorLastName();
        return doctor;
    }
    public LocalDate getCustomVisitDate()  {
        Date visitDateTime =getVisitDateTime();
        LocalDate visitDate = visitDateTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return visitDate;
    }
    public LocalTime getCustomVisitTime()  {
        Date visitDateTime =getVisitDateTime();
        LocalTime visitTime = visitDateTime.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
        return visitTime;
    }


    @Override
    public String toString() {
        return "CustomAllRegisteredVisitsList{" +
                "visitDateTime=" + visitDateTime +
                ", profTitle='" + profTitle + '\'' +
                ", doctorFirstName='" + doctorFirstName + '\'' +
                ", doctorLastName='" + doctorLastName + '\'' +
                ", specialization='" + specialization + '\'' +
                '}';
    }
}


