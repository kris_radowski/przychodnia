package com.example.aplikacja_przychodnia.DataModelAndRepo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    @Query("SELECT DISTINCT d.specialization FROM Doctor d")
    List<String> findAllSpecializations();

//    @Query("SELECT DISTINCT d.user FROM Doctor d")
    @Query(value = "SELECT distinct last_name FROM user WHERE email!='admin@gmail.com'",nativeQuery = true)
    List<String> findAllUsernames();

    DoctorDTO findAllBySpecialization(String specialization);

    DoctorDTO findByUserAndSpecialization(User user, String specialization);

    @Query(value = "SELECT prof_title FROM przychodnia.doctor where user_id=:userId ",nativeQuery = true)
    String findDoctorTitleForUser(Long userId);

    @Transactional
    @Query(value = "SELECT \n" +
            "doctor.id, doctor.prof_title,doctor.specialization ,user.*\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id\n" +
            "AND doctor.specialization=:specialization ",
            countQuery = "SELECT count(user.email)\n" +
                    "FROM przychodnia.doctor, przychodnia.user\n" +
                    "where doctor.user_id=user.user_id \n" +
                    "AND doctor.specialization=:specialization ",
            nativeQuery = true)
    Page<Doctor> findDoctorsBySpecialization(String specialization,Pageable pageable);

    @Transactional
    @Query(value = "SELECT \n" +
            "doctor.id, doctor.prof_title,doctor.specialization ,user.*\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id " +
            "AND user.last_name=:lastName",
            countQuery = "SELECT count(user.email)\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id " +
                    "AND user.last_name=:lastName",
            nativeQuery = true)
    Page<Doctor> findDoctorsByLastName(String lastName,Pageable pageable);

    @Transactional
    @Query(value = "SELECT doctor.id, doctor.prof_title,doctor.specialization ,user.* \n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id \n" +
            "AND user.last_name=:lastName \n" +
            "AND doctor.specialization=:specialization ",
            countQuery = "SELECT count(user.email)\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id \n" +
            "AND user.last_name=:lastName \n" +
            "AND doctor.specialization=:specialization ",
    nativeQuery = true)
    Page<Doctor> findAllByLastNameAndSpecialization(String lastName, String specialization, Pageable pageable);

    @Transactional
    @Query(value = "SELECT \n" +
            "doctor.id, doctor.prof_title,doctor.specialization ,user.*\n" +
            "FROM przychodnia.doctor, przychodnia.user\n" +
            "where doctor.user_id=user.user_id\n",
            countQuery = "SELECT  count(user.email) \n" +
            "FROM przychodnia.doctor, przychodnia.user \n" +
            "where doctor.user_id=user.user_id",
            nativeQuery = true)
    Page<Doctor> findAllDoctors(Pageable pageable);

    @Query(value = "select * from doctor where doctor.id=:doctorId",nativeQuery = true)
    Doctor findDoctorById(Long doctorId);

    @Query(value = "select doctor.id from doctor, user where doctor.user_id=user.user_id\n" +
            "AND user.email=:email",nativeQuery = true)
    Long findDoctorIdByEmail(String email);
}
