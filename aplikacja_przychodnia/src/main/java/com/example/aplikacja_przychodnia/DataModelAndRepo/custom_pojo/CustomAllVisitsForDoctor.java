package com.example.aplikacja_przychodnia.DataModelAndRepo.custom_pojo;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * model danych do wyświetlania wszystkich wizyt lekarskich umówionych do danego lekarza - to ma być użyte w widoku lekarza
 * */
public class CustomAllVisitsForDoctor {
    private Date visitDateTime;
    private String patientFirstName;
    private String patientLastName;
    private String description;
    private Long userId;
    private boolean isFinished;

    public CustomAllVisitsForDoctor(Date visitDateTime, String patientFirstName, String patientLastName, String description, Long userId, boolean isFinished) {
        this.visitDateTime = visitDateTime;
        this.patientFirstName = patientFirstName;
        this.patientLastName = patientLastName;
        this.description = description;
        this.userId = userId;
        this.isFinished = isFinished;
    }

    public Date getVisitDateTime() {
        return visitDateTime;
    }

    public void setVisitDateTime(Date visitDateTime) {
        this.visitDateTime = visitDateTime;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isFinished() { return isFinished; }

    public void setFinished(boolean finished) { isFinished = finished; }

    public LocalDate getCustomVisitDate()  {
        Date visitDateTime =getVisitDateTime();
        LocalDate visitDate = visitDateTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return visitDate;
    }
    public LocalTime getCustomVisitTime()  {
        Date visitDateTime =getVisitDateTime();
        LocalTime visitTime = visitDateTime.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
        return visitTime;
    }
    public String getCustomPatientFormatted() {

        String patient = patientFirstName+" "+patientLastName;
        return patient;
    }

    @Override
    public String toString() {
        return "CustomAllVisitsForDoctor{" +
                "visitDateTime=" + visitDateTime +
                ", patientFirstName='" + patientFirstName + '\'' +
                ", patientLastName='" + patientLastName + '\'' +
                '}';
    }
}

