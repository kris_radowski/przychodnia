package com.example.aplikacja_przychodnia.DataModelAndRepo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findByRole(String role);

    @Query("SELECT u.role FROM UserRole u where u.description=?1")
    String findUserRoleByDescription(String description);



}
